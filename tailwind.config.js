module.exports = {
  purge: [],
  purge: [
    './components/**/*.{vue,js}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './plugins/**/*.{js,ts}',
    './nuxt.config.{js,ts}',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        typeBorder: '#0000008f',
        brown: {
          500: '#e4e0cf',
          400: '#f5f3e9',
        },
        white: {
          full: '#ffff',
          500: '#e8e8e8',
          400: '#f3f3f3',
        },
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
