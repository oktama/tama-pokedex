export default {
  methods: {
    getNameAndId(data) {
      if (data.length >= 1 && data !== null) {
        const result = []
        data.forEach((element) => {
          result.push({
            name: element.name,
            id: element.id,
          })
        })
        return result
      }
    },
    getPokemonData(data, name) {
      if (data.length >= 1 && data !== null) {
        const result = []
        data.forEach((element) => {
          result.push(element[name].name)
        })
        return result
      }
    },
    setNumberingPokemon(id) {
      const str = '' + id
      const pad = '000'
      const ans = pad.substring(0, pad.length - str.length) + str
      return `#${ans}`
    },
  },
}
